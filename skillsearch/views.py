from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from json_views.views import PaginatedJSONListView, JSONDetailView
from django.db.models import Q
from django.db.models.query import QuerySet
from django.contrib.auth.models import Permission
from .models import Skills, Employees, Candidate, EmployeeSkills, CandidateSkills, Review, Salary, CandidateSalary, Interview, EmployeeInterview
from django.urls import reverse
import re

class IndexView(generic.RedirectView):
    pattern_name = 'index_redirect'

class SkillsView(LoginRequiredMixin, generic.ListView):
    template_name = 'skillsearch/skills.html'
    context_object_name = 'skills'
    model = Skills
    login_url = '/'
    redirect_field_name = 'redirect_to'

    def get_queryset(self):
        return Skills.objects.filter().order_by('id')

class EmployeesView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'skillsearch/employees.html'
    context_object_name = 'employees1'
    login_url = '/'
    redirect_field_name = 'redirect_to'

class DetailView(LoginRequiredMixin, generic.DetailView):

    def add_context(self, context, model):
        permissions = [p.codename for p in Permission.objects.filter(user=self.request.user)]
        if all(x in permissions for x in ['add_' + model.__name__.lower(), 'change_' + model.__name__.lower(), 'delete_' + model.__name__.lower()]) or self.request.user.is_superuser:
            context = self.try_keys(model, context)
        return context

    def try_keys(self, model, context):
        keys = {
            'employee_id': Q(employee_id=context['object'].id),
            'candidate_id': Q(candidate_id=context['object'].id)
        }
        for key in keys:
            try:
                context[model.__name__.lower()] = model.objects.get(keys[key])
                return context
            except Exception:
                continue
        return context

class CandidatesView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'skillsearch/candidates.html'
    context_object_name = 'candidates'
    login_url = '/'
    redirect_field_name = 'redirect_to'

class CoreSearchMixin():

    def __init__(self):
        self.order = ('-active', '-id') if hasattr(self.model, 'active') else ('-id',)

    def query(self):
        '''
        Should be run in get_queryset
        :return:
        '''
        if 'searchstring' in self.request.GET and 'condition' in self.request.GET:
            search = self.request.GET['searchstring']
            condition = self.request.GET['condition']
            query_result = self.return_employees(search, condition, 'skillset__name')
            return query_result
        return self.model.objects.all().order_by(*self.order)

    def return_employees(self, search, condition, filter_name):
        '''
        Common method for search with different conditions
        :param search:
        :param condition:
        :return:
        '''
        q = Q()  # query object
        if condition == 'or':
            search_list = self.quotes_match(search)
            for value in search_list:
                #Exact search
                if value[0] in ["'", '"'] or value[-1] in ["'", '"']:
                    filter = filter_name + '__iexact'
                    value = value[1:-1]
                #Approximate search
                else:
                    filter = filter_name + '__icontains'
                q = q | self.filter_by_name(q, filter, value)
            return self.model.objects.filter(q).distinct().order_by(*self.order)
        elif condition == 'and':
            e = False
            search_list = self.quotes_match(search)
            for value in search_list:
                #Exact search
                if value[0] in ["'", '"'] or value[-1] in ["'", '"']:
                    filter = filter_name + '__iexact'
                    value = value[1:-1]
                #Approximate search
                else:
                    filter = filter_name + '__icontains'
                e = self.filter_by_name(e, filter, value)
            if e or isinstance(e, QuerySet):
                return e.distinct().order_by(*self.order)
        return self.model.objects.all()

    def quotes_match(self, search):
        '''
        Method searches for search data in quotes in order to handle exact search with spaces correctly
        :param search:
        :return:
        '''
        matches = re.findall(r'\"(.+?)\"', search)
        for quote in ['"', "'"]:
            for match in matches:
                search = search.replace(quote + match + quote, '')
        search_list = search.split()
        return search_list + matches

    def filter_by_name(self, object, name, value):
        # Q
        if isinstance(object, Q):
            filters = {
                'skillset__name__iexact': Q(skillset__name__iexact=value),
                'skillset__name__icontains': Q(skillset__name__icontains=value),
                'first_name__iexact': Q(first_name__iexact=value),
                'first_name__icontains': Q(first_name__icontains=value),
                'last_name__iexact': Q(last_name__iexact=value),
                'last_name__icontains': Q(last_name__icontains=value),
                'team__name__iexact': Q(teams__name__iexact=value),
                'team__name__icontains': Q(teams__name__icontains=value),
                'position__name__iexact': Q(position__name__iexact=value),
                'position__name__icontains': Q(position__name__icontains=value)
            }
        #E
        elif isinstance(object, QuerySet):
            filters = {
                'skillset__name__iexact': object.filter(skillset__name__iexact=value),
                'skillset__name__icontains': object.filter(skillset__name__icontains=value),
                'team__name__iexact': object.filter(teams__name__iexact=value),
                'team__name__icontains': object.filter(teams__name__icontains=value),
                'position__name__iexact': object.filter(position__name__iexact=value),
                'position__name__icontains': object.filter(position__name__icontains=value)
            }
        #E first
        elif object is False:
            filters = {
                'skillset__name__iexact' : self.model.objects.filter(skillset__name__iexact=value),
                'skillset__name__icontains': self.model.objects.filter(skillset__name__icontains=value),
            }
            if name[0:5] == 'teams':
                filters['team__name__iexact'] = self.model.objects.filter(teams__name__iexact=value)
                filters['team__name__icontains'] = self.model.objects.filter(teams__name__icontains=value)
            if name[0:8] == 'position':
                filters['position__name__iexact'] = self.model.objects.filter(position__name__iexact=value)
                filters['position__name__icontains'] = self.model.objects.filter(position__name__icontains=value)
        return filters[name]

class SkillsearchJSONView(LoginRequiredMixin, PaginatedJSONListView):
    '''
    Parent view for skillsearch employee views
    '''

    paginate_by = 18
    count_query = 'count'
    count_only  = False
    login_url = '/'
    redirect_field_name = 'redirect_to'

class EmployeesJSONView(CoreSearchMixin, SkillsearchJSONView):
    model = Employees

    def get_queryset(self):
        return self.query()

class CandidatesJSONView(CoreSearchMixin, SkillsearchJSONView):
    model = Candidate

    def get_queryset(self):
        return self.query()

class AdvancedSearchMixin(CoreSearchMixin):
    '''
    Mixin for advanced search
    '''
    def query(self):
        search_by = self.request.GET['search_by']
        searchstring = self.request.GET['searchstring']
        condition = self.request.GET['condition']
        q = Q() #query object
        # Search by name
        if search_by == 'name':
            #Common method cannot make two iternations with Q so far
            search_list = self.quotes_match(searchstring)
            for value in search_list:
                if value[0] in ["'", '"'] or value[-1] in ["'", '"']:
                    q = q | self.filter_by_name(q, 'first_name__iexact', value[1:-1])
                    q = q | self.filter_by_name(q, 'last_name__iexact', value[1:-1])
                else:
                    q = q | self.filter_by_name(q, 'first_name__icontains', value)
                    q = q | self.filter_by_name(q, 'last_name__icontains', value)
            return self.model.objects.filter(q).distinct().order_by(*self.order)
        #Search by team
        elif search_by == 'team':
            team_query_result = self.return_employees(searchstring, condition, 'teams__name')
            if team_query_result or isinstance(team_query_result, QuerySet):
                return team_query_result
        elif search_by == 'position':
            pos_query_result = self.return_employees(searchstring, condition, 'position__name')
            if pos_query_result or isinstance(pos_query_result, QuerySet):
                return pos_query_result
        return self.model.objects.all()

class AdvancedTestMixin(CoreSearchMixin):
    def query(self):
        q = Q() #creating query object
        criterias = [
            'name',
            'team',
            'position',
        ]
        for criteria in criterias:
            #getting lists of parameters for each search criteria
            search_list = self.request.GET.getlist(criteria)
            print(search_list)
            #loop for fields
            for item in search_list:
                #loop for each entered parameter in search criteria
                print(item)
                if item:
                    for value in item.split():
                        if criteria == 'name':
                            if value[0] in ["'", '"'] or value[-1] in ["'", '"']:
                                q = q | self.filter_by_name(q, 'first_name__iexact', value[1:-1])
                                q = q | self.filter_by_name(q, 'last_name__iexact', value[1:-1])
                            else:
                                q = q | self.filter_by_name(q, 'first_name__icontains', value)
                                q = q | self.filter_by_name(q, 'last_name__icontains', value)
                        else:
                            if value[0] in ["'", '"'] or value[-1] in ["'", '"']:
                                q = q | self.filter_by_name(q, criteria + '__name__iexact', value[1:-1])
                            else:
                                q = q | self.filter_by_name(q, criteria + '__name__icontains', value)
                    q = q & Q(q)
        return self.model.objects.filter(q).distinct().order_by(*self.order)
        # return self.model.objects.all()

class SkillsearchAdvancedJSONView(SkillsearchJSONView):
    '''
    This view is used to handle advanced search
    '''

    def get_queryset(self):
        search_by = self.request.GET['search_by']
        searchstring = self.request.GET['searchstring']
        condition = self.request.GET['condition']
        q = Q() #query object
        # Search by name
        if search_by == 'name':
            #Common method cannot make two iternations with Q so far
            search_list = self.quotes_match(searchstring)
            for value in search_list:
                if value[0] in ["'", '"'] or value[-1] in ["'", '"']:
                    q = q | self.filter_by_name(q, 'first_name__iexact', value[1:-1])
                    q = q | self.filter_by_name(q, 'last_name__iexact', value[1:-1])
                else:
                    q = q | self.filter_by_name(q, 'first_name__icontains', value)
                    q = q | self.filter_by_name(q, 'last_name__icontains', value)
            return self.model.objects.filter(q).distinct().order_by(*self.order)
        #Search by team
        elif search_by == 'team':
            team_query_result = self.return_employees(searchstring, condition, 'teams__name')
            if team_query_result or isinstance(team_query_result, QuerySet):
                return team_query_result
        elif search_by == 'position':
            pos_query_result = self.return_employees(searchstring, condition, 'position__name')
            if pos_query_result or isinstance(pos_query_result, QuerySet):
                return pos_query_result
        return self.model.objects.all()

class ComplexSearchMixin(CoreSearchMixin):

    def query(self):
        e = False
        for key, search in self.request.GET.items():
            q = Q()
            #Getting values from each search imput field
            if key[0:6] == 'search':
                #Getting each value specified in search field separated by space
                search_list = self.quotes_match(search)
                for value in search_list:
                    # Exact search
                    if value[0] in ["'", '"'] or value[-1] in ["'", '"']:
                        filter = 'skillset__name__iexact'
                        value = value[1:-1]
                    # Approximate search
                    else:
                        filter = 'skillset__name__icontains'
                    q = q | self.filter_by_name(q, filter, value)
                q = q & Q(q)
            if e or isinstance(e, QuerySet):
                e = e.filter(q)
            else:
                e = self.model.objects.filter(q)
        return e.distinct().order_by(*self.order)

class EmployeesAdvancedJSONView(AdvancedTestMixin, SkillsearchJSONView):
    model = Employees

    def get_queryset(self):
        return self.query()

class CandidatesAdvancedJSONView(AdvancedSearchMixin, SkillsearchJSONView):
    model = Candidate

    def get_queryset(self):
        return self.query()

class EmployeesComplexJSONView(ComplexSearchMixin, SkillsearchJSONView):
    model = Employees

    def get_queryset(self):
        return self.query()

class CandidateComplexJSONView(ComplexSearchMixin, SkillsearchJSONView):
    model = Candidate

    def get_queryset(self):
        return self.query()

class CEJSONDetailView(LoginRequiredMixin, JSONDetailView):

    def add_context(self, context, model):
        permissions = [p.codename for p in Permission.objects.filter(user=self.request.user)]
        if all(x in permissions for x in ['add_' + model.__name__.lower(), 'change_' + model.__name__.lower(), 'delete_' + model.__name__.lower()]) or self.request.user.is_superuser:
            context = self.try_keys(model, context)
        return context

    def try_keys(self, model, context):
        keys = {
            'employee_id': Q(employee_id=context['profiles'].id),
            'candidate_id': Q(candidate_id=context['profiles'].id)
        }
        for key in keys:
            try:
                context[model.__name__.lower()] = model.objects.get(keys[key])
                return context
            except Exception:
                continue
        return context

class EmployeeJSONDetailView(CEJSONDetailView):
    model = Employees
    login_url = '/'

    def get_context_data(self, **kwargs):
        permissions = [p.codename for p in Permission.objects.filter(user=self.request.user)]
        context = super(EmployeeJSONDetailView, self).get_context_data(**kwargs)
        context['profiles'] = context.pop('employees')
        context['interviews'] = EmployeeInterview.objects.filter(employee=context['profiles'].id).order_by('-date')[:5]
        context['reviews'] = Review.objects.filter(employee=context['profiles'].id).order_by('-date')[:5]
        context = self.add_context(context, Salary)
        context['interviews_link'] = reverse('skillsearch:einterviews', args=(context['profiles'].id,))
        context['reviews_link'] = reverse('skillsearch:reviews', args=(context['profiles'].id,))
        return context

class CandidateJSONDetailView(CEJSONDetailView):
    model = Candidate
    login_url = '/'

    def get_context_data(self, **kwargs):
        context = super(CandidateJSONDetailView, self).get_context_data(**kwargs)
        context['profiles'] = context.pop('candidate')
        context = self.add_context(context, CandidateSalary)
        context['interviews'] = Interview.objects.filter(candidate=context['profiles'].id).order_by('-date')[:5]
        context['profile_id'] = context['profiles'].id
        context['interviews_link'] = reverse('skillsearch:interviews', args=(context['profiles'].id,))
        return context

class EmployeeDetailView(DetailView):
    model = Employees
    login_url = '/'
    template_name = 'skillsearch/employee_details.html'

    def get_context_data(self, **kwargs):
        permissions = [p.codename for p in Permission.objects.filter(user=self.request.user)]
        context = super(EmployeeDetailView, self).get_context_data(**kwargs)
        # context = self.add_context(context, Review.objects.filter(employee=context['employees'].id))
        context['reviews'] = Review.objects.filter(employee=context['employees'].id).order_by('-date')[:5]
        context = self.add_context(context, Salary)
        context['interviews'] = EmployeeInterview.objects.filter(employee=context['employees'].id).order_by('-date')[:5]
        context['profile_id'] = context['employees'].id
        context['interviews_link'] = reverse('skillsearch:einterviews', args=(context['profile_id'],))
        context['reviews_link'] = reverse('skillsearch:reviews', args=(context['profile_id'],));
        return context

class CandidateDetailView(DetailView):
    model = Candidate
    login_url = '/'
    template_name = 'skillsearch/candidate_details.html'

    def get_context_data(self, **kwargs):
        context = super(CandidateDetailView, self).get_context_data(**kwargs)
        permissions = [p.codename for p in Permission.objects.filter(user=self.request.user)]
        context = self.add_context(context, CandidateSalary)
        context['hire_url'] = reverse('skillsearch:hire', args=(context['candidate'].id,))
        context['interviews'] = Interview.objects.filter(candidate=context['candidate'].id).order_by('-date')[:5]
        context['profile_id'] = context['candidate'].id
        context['interviews_link'] = reverse('skillsearch:interviews', args=(context['profile_id'],))
        return context

class ReviewListView(LoginRequiredMixin, generic.ListView):
    model = Review
    login_url = '/'
    template_name = 'skillsearch/reviews.html'
    context_object_name = 'reviews'

    def get_queryset(self):
        if 'employee_id' in self.kwargs:
            return Review.objects.filter(employee__exact=self.kwargs['employee_id']).order_by('-date')
        else:
            return Review.objects.order_by('-date')

    def get_context_data(self, **kwargs):
        context = super(ReviewListView, self).get_context_data(**kwargs)
        if 'employee_id' in self.kwargs:
            context['profile'] = Employees.objects.get(id=self.kwargs['employee_id'])
        return context

class ReviewDetailView(LoginRequiredMixin, generic.DetailView):
    model = Review
    login_url = '/'
    template_name = 'skillsearch/review.html'
    context_object_name = 'review'

    def get_context_data(self, **kwargs):
        context = super(ReviewDetailView, self).get_context_data(**kwargs)
        context['profile'] = context['review'].employee
        return context

class InterviewListView(LoginRequiredMixin, generic.ListView):
    model = Interview
    login_url = '/'
    template_name = 'skillsearch/interviews.html'
    context_object_name = 'interviews'

    def get_queryset(self):
        if 'candidate_id' in self.kwargs:
            return Interview.objects.filter(candidate__exact=self.kwargs['candidate_id']).order_by('-date')
        else:
            return Interview.objects.order_by('-date')
    
    def get_context_data(self, **kwargs):
        context = super(InterviewListView, self).get_context_data(**kwargs)
        if 'candidate_id' in self.kwargs:
            context['profile'] = Candidate.objects.get(id=self.kwargs['candidate_id'])
        return context


class EmployeeInterviewListView(LoginRequiredMixin, generic.ListView):
    model = EmployeeInterview
    login_url = '/'
    template_name = 'skillsearch/interviews.html'
    context_object_name = 'interviews'

    def get_queryset(self):
        if 'employee_id' in self.kwargs:
            return EmployeeInterview.objects.filter(employee__exact=self.kwargs['employee_id']).order_by('-date')
        else:
            return EmployeeInterview.objects.order_by('-date')

    def get_context_data(self, **kwargs):
        context = super(EmployeeInterviewListView, self).get_context_data(**kwargs)
        if 'employee_id' in self.kwargs:
            context['profile'] = Employees.objects.get(id=self.kwargs['employee_id'])
        return context

class InterviewDetailView(LoginRequiredMixin, generic.DetailView):
    model = Interview
    login_url = '/'
    template_name = 'skillsearch/interview.html'
    context_object_name = 'interview'

    def get_context_data(self, **kwargs):
        context = super(InterviewDetailView, self).get_context_data(**kwargs)
        context['profile'] = context['interview'].candidate
        return context

class EmployeeInterviewDetailView(LoginRequiredMixin, generic.DetailView):
    model = EmployeeInterview
    login_url = '/'
    template_name = 'skillsearch/interview.html'
    context_object_name = 'interview'

    def get_context_data(self, **kwargs):
        context = super(EmployeeInterviewDetailView, self).get_context_data(**kwargs)
        context['profile'] = context['interview'].employee
        return context

class HireCandidateView(LoginRequiredMixin, generic.RedirectView):
    permanent = False
    query_string = True
    pattern_name = 'employeedetailspage'

    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.has_perm('skillsearch.delete_candidate') and self.request.user.has_perm('skillsearch.add_employees'):
            candidate = Candidate.objects.get(id=kwargs['candidate_id'])
            candidate_interview = Interview.objects.filter(candidate__exact=kwargs['candidate_id'])
            employee = Employees()
            employee.first_name = candidate.first_name
            employee.last_name = candidate.last_name
            employee.birthday = candidate.birthday
            employee.phone_number = candidate.phone_number
            employee.pemail = candidate.email
            employee.skype = candidate.skype
            employee.cv = candidate.cv
            employee.picture = candidate.picture
            employee.thumbnail = candidate.thumbnail
            employee.added_by = candidate.added_by
            employee.save()

            #Transfer interviews
            if candidate_interview:
                for interview in candidate_interview:
                    einterview = EmployeeInterview()
                    einterview.employee_id = employee.id
                    einterview.date = interview.date
                    einterview.short_description = interview.short_description
                    einterview.description = interview.description
                    einterview.result = interview.result
                    einterview.save()
                    einterview.interviewers = interview.interviewers.all()
                    einterview.save()

            cskills = CandidateSkills.objects.filter(candidates__exact=candidate.id)
            for skill in cskills:
                eskill = EmployeeSkills()
                eskill.employees = employee
                eskill.skills = skill.skills
                eskill.level = skill.level
                eskill.save()
            candidate.delete()
            self.url = reverse('skillsearch:employeedetailspage', kwargs={'pk': employee.id})
        return super(HireCandidateView, self).get_redirect_url(*args, **kwargs)
