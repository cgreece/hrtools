from django.test import TestCase, Client
from django.core.files.uploadedfile import SimpleUploadedFile
from django.conf import settings
from .models import Skills, Employees, EmployeeSkills, Team, Candidate, Position, CandidateSkills, Review, Salary, CandidateSalary, Interview, InterviewResult, EmployeeInterview, ComplianceResult
from django.contrib.auth.models import User, Permission
from django.urls import reverse
from django.db.models.query import QuerySet
from .client import Client as CustomClient
from random import randint
from random import sample
from datetime import datetime

import json
import os

#Models

#Employee model

class EmployeesModelBaseTestCase(TestCase):
    '''
    Parent class which creates employee with all available fields
    '''

    def setUp(self):
        # Creating test positions and teams
        for pt in range(1, 3):
            position = Position()
            position.name = 'Test' + str(pt)
            position.save()
            team = Team()
            team.name = 'Test' + str(pt)
            team.save()
        #Creating compliance result
        compliance_result = ComplianceResult()
        compliance_result.name = 'passed'
        compliance_result.color = 'green'
        compliance_result.save()
        # Creating test user
        self.username = 'testuser'
        self.userpassword = 'thisistestuserpassword123'
        self.user = User.objects.create_user(self.username)
        self.user.set_password(self.userpassword)
        self.user.save()
        # Creating employee
        self.employee = Employees(first_name='Test', last_name='Case')
        self.employee.picture = SimpleUploadedFile(name='test.jpg', content=open(
            os.path.join(settings.BASE_DIR, 'skillsearch/static/skillsearch/images/test.jpg'), 'rb').read(),
                                                   content_type='image/jpeg')
        self.employee.compliance = ComplianceResult.objects.get(id=1)
        self.employee.birthday = datetime.now()
        self.employee.phone_number = '+380997776655'
        self.employee.skype = 'test_skype'
        self.employee.cv = SimpleUploadedFile(name='test.pdf', content=open(
            os.path.join(settings.BASE_DIR, 'skillsearch/static/skillsearch/test/McDonald\'s Ukraine.pdf'),
            'rb').read(), content_type='document/pdf')
        self.employee.added_by = User.objects.get(id=1)
        self.employee.facebook = 'https://www.facebook.com/test_profile.1'
        self.employee.linkedin = 'https://www.linkedin.com/in/test'
        self.employee.twitter = 'https://twitter.com/Test'
        self.employee.pemail = 'test@personalemail.com'
        self.employee.cemail = 'test@corporateemail.com'
        # Saving to be able to use many-to-many
        self.employee.save()
        self.employee.position = Position.objects.all()
        # active field is True by default
        self.employee.teams = Team.objects.all()
        self.employee.save()
        # Creating skills and manytomany relation
        for i in range(1, 3):
            skill = Skills(name='skill' + str(i))
            skill.save()
            employeeskill = EmployeeSkills()
            employeeskill.employees = self.employee
            employeeskill.skills = skill
            employeeskill.save()
            level = EmployeeSkills.objects.get(id=i)
            level.level = i
            level.save()
        self.employee.save()

class EmployeesSerializeTestCase(EmployeesModelBaseTestCase):
    '''
    Tesing serialize function. All employee fields should be filled in.
    '''
    def setUp(self):
        super(EmployeesSerializeTestCase, self).setUp()

    def test_serialize(self):
        result = self.employee.serialize()
        self.assertIsInstance(result, dict)
        for field, value in result.items():
            self.assertTrue(value)

class EmployeesGetLevelsTestCase(EmployeesModelBaseTestCase):
    '''
    Testing get_levels method.
    '''
    def setUp(self):
        super(EmployeesGetLevelsTestCase, self).setUp()

    def test_get_levels(self):
        result = self.employee.get_levels()
        self.assertIsInstance(result, list)
        self.assertEqual(result[0], 1)
        self.assertEqual(result[1], 2)

class EmployeesNameTestCase(TestCase):
    '''
    Testing all name-relate stuff for Employees model
    '''
    def setUp(self):
        self.employee = Employees(first_name='Test', last_name='Case')
        self.employee.save()

    def test_str(self):
        self.assertEqual(self.employee.__str__(), self.employee.first_name + ' ' + self.employee.last_name)

    def test_length(self):
        self.assertEqual(self.employee._meta.get_field('first_name').max_length, 100)
        self.assertEqual(self.employee._meta.get_field('last_name').max_length, 100)

class EmployeesThumbTestCase(TestCase):
    '''
    Testing creation of thumbnails
    '''

    def setUp(self):
        self.employee = Employees(first_name='Test', last_name='Case')
        self.count_media_before = len(os.listdir(settings.MEDIA_ROOT))
        self.employee.save()

    def test_thumb(self):
        '''
        Function checks whether default thumbnail was changed
        :return:
        '''
        self.employee.picture = SimpleUploadedFile(name='test.jpg', content=open(
            os.path.join(settings.BASE_DIR, 'skillsearch/static/skillsearch/images/test.jpg'), 'rb').read(),
                                                   content_type='image/jpeg')
        self.employee.save()
        employee = Employees.objects.get(id=1)
        #Check whether thumbnail value in DB is changed from default
        self.assertNotEqual(employee.thumbnail.name, './plug.png')
        #Check whether files are created
        count_media_after = len(os.listdir(settings.MEDIA_ROOT))
        #Two files should be added - full profile picture and thumbnail
        self.assertEqual(count_media_after, self.count_media_before + 2)

        os.remove(self.employee.picture.path)
        os.remove(self.employee.thumbnail.path)

    def test_empty_thumb(self):
        '''
        No thumbnail should be created if picture is not specified.
        Just plug.png should be set as picture by default.
        :return:
        '''

        employee = Employees.objects.get(id=1)
        count_media_after = len(os.listdir(settings.MEDIA_ROOT))
        #Check if thumbnail is set to default value
        self.assertEqual(employee.thumbnail.name, './plug.png')
        #Check if no files were created in media root directory
        self.assertEqual(self.count_media_before, count_media_after)

#Candidate model

class CandidateModelBaseTestCase(TestCase):

    def setUp(self):
        self.candidate = Candidate(first_name='Test', last_name='Case')
        self.candidate.email = 'testemail@testaddress.com'
        self.candidate.save()
        # Creating skills and manytomany relation
        for i in range(1, 3):
            skill = Skills(name='skill' + str(i))
            skill.save()
            candidateskill = CandidateSkills()
            candidateskill.candidates = self.candidate
            candidateskill.skills = skill
            candidateskill.save()


class CandidateModelTestCase(CandidateModelBaseTestCase):
    '''
    Testing candidate serialize and name
    '''
    def setUp(self):
        super(CandidateModelTestCase, self).setUp()

    def test_serialize(self):
        '''
        Testing candidate serialize method output
        Checking only 2 fields - email and skillset. All other fields are common and tested for employee.
        :return:
        '''
        result = self.candidate.serialize()
        self.assertIsInstance(result, dict)
        self.assertEqual(result['email'], self.candidate.email)
        #Checking whether skillset dict is not empty
        self.assertTrue(result['skillset'])
        #Checking that there are exactly two skills
        self.assertEqual(len(result['skillset']), 2)

    def test_str(self):
        self.assertEqual(self.candidate.__str__(), self.candidate.first_name + ' ' + self.candidate.last_name)

    def test_length(self):
        self.assertEqual(self.candidate._meta.get_field('first_name').max_length, 100)
        self.assertEqual(self.candidate._meta.get_field('last_name').max_length, 100)

#Review model

class ReviewTestCase(EmployeesModelBaseTestCase):
    '''
    Testing reviews serialization and name.
    '''
    def setUp(self):
        super(ReviewTestCase, self).setUp()
        self.review = Review()
        self.review.employee = self.employee
        self.review.responsilities_feedback = 'Some test feedback.'
        self.review.responsilities_promises = 'Some test promises.'
        self.review.team_feedback = 'Some test team feedback.'
        self.review.team_promises = 'Some test team promises.'
        self.review.office_feedback = 'Some test office feedback.'
        self.review.office_promises = 'Some test office promises.'
        self.review.other_feedback = 'Some test other feedback'
        self.review.other_promises = 'Some test other promises.'
        self.review.save()

    def test_review_serialize(self):
        result = self.review.serialize()
        self.assertIsInstance(result, dict)
        self.assertNotEqual(result['id'], None)
        self.assertEqual(result['responsilities_feedback'], self.review.responsilities_feedback)
        self.assertEqual(result['responsilities_promises'], self.review.responsilities_promises)
        self.assertEqual(result['team_feedback'], self.review.team_feedback)
        self.assertEqual(result['team_promises'], self.review.team_promises)
        self.assertEqual(result['office_feedback'], self.review.office_feedback)
        self.assertEqual(result['office_promises'], self.review.office_promises)
        self.assertEqual(result['other_feedback'], self.review.other_feedback)
        self.assertEqual(result['other_promises'], self.review.other_promises)

    def test_review_name(self):
        self.assertEqual(self.review.__str__(), self.review.employee.first_name + ' ' + self.review.employee.last_name)

class SalaryTestCase(EmployeesModelBaseTestCase):
    '''
    Testing salary serialization.
    '''
    def setUp(self):
        super(SalaryTestCase, self).setUp()
        self.salary = Salary()
        self.salary.employee = self.employee
        self.salary.current_salary = 1000
        self.salary.desired_salary = 2000
        self.salary.save()

    def test_salary_serialize(self):
        result = self.salary.serialize()
        self.assertIsInstance(result, dict)
        self.assertNotEqual(result['id'], None)
        self.assertEqual(result['desired_salary'], self.salary.desired_salary)
        self.assertEqual(result['current_salary'], self.salary.current_salary)

    def test_salary_name(self):
        self.assertEqual(self.salary.__str__(),
                         self.salary.employee.first_name + ' ' + self.salary.employee.last_name)

class CandidateSalaryTestCase(CandidateModelBaseTestCase):
    '''
    Testing candidate salary serialization and name.
    '''
    def setUp(self):
        super(CandidateSalaryTestCase, self).setUp()
        self.salary = CandidateSalary()
        self.salary.candidate = self.candidate
        self.salary.salary = 2000
        self.salary.save()

    def test_candidate_serialize(self):
        result = self.salary.serialize()
        self.assertIsInstance(result, dict)
        self.assertNotEqual(result['id'], None)
        self.assertEqual(result['salary'], self.salary.salary)

    def test_candidate_salary_name(self):
        self.assertEqual(self.salary.__str__(),
                         self.salary.candidate.first_name + ' ' + self.salary.candidate.last_name)
        
class InterviewModelTestCase(CandidateModelBaseTestCase):
    '''
    Testing interview (candidates) serialization, name and get_link method.
    '''
    def setUp(self):
        super(InterviewModelTestCase, self).setUp()
        #Creating employee for interviewer field
        interviewer = Employees(first_name='Test', last_name='Case')
        interviewer.save()
        #Creating team
        team = Team()
        team.name = 'Test1'
        team.save()
        #Creating interview result
        interviewresult = InterviewResult()
        interviewresult.name = 'success'
        interviewresult.save()
        #Creating interview
        self.interview = Interview()
        self.interview.candidate = self.candidate
        self.interview.date = datetime.now()
        self.interview.short_description = 'Test short description.'
        self.interview.description = 'Test long description.'
        self.interview.result = interviewresult
        self.interview.save()
        self.interview.teams = Team.objects.all()
        self.interview.interviewers = Employees.objects.all()
        self.interview.save()

    def test_interview_serialization(self):
        result = self.interview.serialize()
        self.assertIsInstance(result, dict)
        self.assertNotEqual(result['id'], None)
        self.assertEqual(result['date'], datetime.now().strftime('%b. %d, %Y'))
        self.assertEqual(result['short_description'], self.interview.short_description)
        self.assertEqual(result['description'], self.interview.description)
        self.assertEqual(result['result'], self.interview.result)

    def test_interview_name_get_link(self):
        '''
        Checking name and get_link together to speed up test a bit.
        :return:
        '''
        self.assertEqual(self.interview.__str__(),
                         self.interview.candidate.first_name + ' ' + self.interview.candidate.last_name)
        self.assertEqual(self.interview.get_link(), reverse('skillsearch:interview', args=(self.interview.id,)))

class InterviewEmployeeModelTestCase(EmployeesModelBaseTestCase):
    def setUp(self):
        super(InterviewEmployeeModelTestCase, self).setUp()
        # Creating interview result
        interviewresult = InterviewResult()
        interviewresult.name = 'success'
        interviewresult.save()
        #Creating interview
        self.interview = EmployeeInterview()
        self.interview.employee = self.employee
        self.interview.date = datetime.now()
        self.interview.result = interviewresult
        self.interview.save()

    def test_employee_interview_serialize(self):
        '''
        Testing employee interview serialization
        No need to additionally test get_link, serialization includes it.
        :return:
        '''
        result = self.interview.serialize()
        self.assertIsInstance(result, dict)
        self.assertNotEqual(result['id'], None)
        self.assertEqual(result['link'], reverse('skillsearch:einterview', args=(self.interview.id,)))

    def test_employee_interview_name(self):
        self.assertEqual(self.interview.__str__(),
                         self.interview.employee.first_name + ' ' + self.interview.employee.last_name)

#Views

class EmployeesLoggedViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        #Using patched django client because default one probably has bug with response.json
        self.custom_client = CustomClient()
        #Creating test user
        self.username = 'testuser'
        self.userpassword = 'thisistestuserpassword123'
        self.user = User.objects.create_user(self.username)
        self.user.set_password(self.userpassword)
        self.user.save()
        self.salary = Salary()
        self.salary.employee_id = 1
        self.salary.current_salary = 1000
        self.salary.desired_salary = 3000
        self.salary.save()
        self.csalary = CandidateSalary()
        self.csalary.candidate_id = 1
        self.csalary.salary = 1500
        self.csalary.save()
        self.review = Review()
        self.review.employee_id = 1
        self.review.responsilities_feedback = 'blahblah'
        self.review.responsilities_promises = 'blahblah'
        self.review.team_feedback = 'blahblah'
        self.review.team_promises = 'blahblah'
        self.review.office_feedback = 'blahblah'
        self.review.office_promises = 'blahblah'
        self.review.other_feedback = 'blahblah'
        self.review.other_promises = 'blahblah'
        self.review.save()
        #Creating 10 skills and teams
        self.team_names = list()
        self.position_names = list()
        for i in range(10):
            skill = Skills()
            skill.name = 'Skill' + str(i)
            skill.save()
            team = Team()
            team.name = 'Team' + str(i)
            self.team_names.append(team.name)
            team.save()
            position = Position()
            position.name = 'Position' + str(i)
            self.position_names.append(position.name)
            position.save()

        #Creating test employees
        now = datetime.now() #current date for birthdays
        for i in range(50):
            #30 random skills
            employee = Employees()
            candidate = Candidate()
            employee.first_name = 'Emp' + str(i)
            employee.last_name = 'Emp' + str(i)
            candidate.first_name = 'Cand' + str(i)
            candidate.last_name = 'Cand' + str(i)
            candidate.birthday = now.strftime('%Y-%m-%d')
            employee.save()
            candidate.save()
            employee.teams = sample(range(1, 10), randint(1, 8))
            employee.position = sample(range(1, 10), randint(1, 8))
            #Explicitly adding two teams for some employees
            if i > 0:
                if 50 % i == 10:
                    employee.teams == [3, 6]
                    employee.position == [3, 6]
            employee.save

            #Random list of 3 unique values
            for i in sample(range(1, 10), randint(1, 8)):
                skills = Skills.objects.filter(name__contains='Skill' + str(i))
                for skill in skills:
                    employeeskill = EmployeeSkills()
                    candidateskill = CandidateSkills()
                    employeeskill.employees = employee
                    candidateskill.candidates = candidate
                    employeeskill.skills = skill
                    candidateskill.skills = skill
                    employeeskill.save()
                    candidateskill.save()
            employee.save()
            candidate.save()
            #Ensure there are some employees with only 2 skills which are checked by search tests
            employee_12 = Employees()
            employee_12.first_name = 'Emp' + str(i) + 'r'
            employee_12.last_name = 'Emp' + str(i) + 'r'
            employee_12.save()
            for i in [0, 1]:
                skills = Skills.objects.filter(name__contains='Skill' + str(i))
                for skill in skills:
                    employeeskill = EmployeeSkills()
                    employeeskill.employees = employee_12
                    employeeskill.skills = skill
                    employeeskill.save()
        # For complex search
        empl = dict()
        for i in ['C', 'S', 'D']:
            empl[i] = Employees()
            empl[i].first_name = i + 'omplname'
            empl[i].last_name = i + 'omplelastname'
            empl[i].save()
        skill_names = ['Compl1', 'Compl2', 'Compl3', 'Compl4', 'SCompl1', 'SCompl2', 'SCompl3', 'DCompl4', 'DCompl2', 'DCompl3', 'DCompl1']
        for name in skill_names:
            skill = Skills()
            skill.name = name
            skill.save()
            empskill = EmployeeSkills()
            if name[0] == 'C':
                empskill.employees = empl['C']
            elif name[0] == 'D':
                empskill.employees = empl['D']
            else:
                empskill.employees = empl['S']
            empskill.skills = skill
            empskill.save()
        #Loggin in
        self.client.login(username=self.username, password=self.userpassword)
        self.custom_client.login(username=self.username, password=self.userpassword)

    def test_loggedin(self):
        response = self.client.get(reverse('skillsearch:employees'))
        self.assertEqual(response.status_code, 200)

    def test_json_loggedin(self):
        response = self.custom_client.get(reverse('skillsearch:employeesjson'))
        self.assertEqual(response.status_code, 200)

    def test_employee_details_loggedin(self):
        response = self.custom_client.get(reverse('skillsearch:employeedetails', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)

    def test_employee_details_info(self):
        response = self.custom_client.get(reverse('skillsearch:employeedetailspage', kwargs={'pk': 1}))
        employee = Employees.objects.get(id=1)
        self.assertContains(response, '<h4 class="empty-tab" id="employee-details-name" align="center">' + employee.first_name + ' ' + employee.last_name + '</h4>')
        self.assertContains(response, '<img class="profile-picture-img rounded" id="profile-picture" src="/media/' + employee.picture.name + '" alt="Card image cap">')
        self.assertContains(response, 'id="team-header">')

    def test_employee_review_no_perms(self):
        response = self.custom_client.get(reverse('skillsearch:employeedetailspage', kwargs={'pk': 1}))
        self.assertNotContains(response, 'id="employee-review-tab-button"')

    def test_employee_review_json_no_perms(self):
        response = self.custom_client.get(reverse('skillsearch:employeedetails', kwargs={'pk': 1}))
        self.assertNotContains(response, '"review":')

    def test_employee_review(self):
        add_review = Permission.objects.get(name='Can add review')
        change_review = Permission.objects.get(name='Can change review')
        delete_review = Permission.objects.get(name='Can delete review')
        self.user.user_permissions.add(add_review, change_review, delete_review)
        response = self.custom_client.get(reverse('skillsearch:employeedetailspage', kwargs={'pk': 1}))
        self.assertContains(response, 'id="employee-review-tab-button"')

    def test_employee_review_json(self):
        add_review = Permission.objects.get(name='Can add review')
        change_review = Permission.objects.get(name='Can change review')
        delete_review = Permission.objects.get(name='Can delete review')
        self.user.user_permissions.add(add_review, change_review, delete_review)
        response = self.custom_client.get(reverse('skillsearch:employeedetails', kwargs={'pk': 1}))
        self.assertContains(response, '"review":')

    def test_employee_candidate_salary_no_perms(self):
        response_emp = self.custom_client.get(reverse('skillsearch:employeedetailspage', kwargs={'pk': 1}))
        response_cand = self.custom_client.get(reverse('skillsearch:candidatedetailspage', kwargs={'pk': 1}))
        self.assertNotContains(response_emp, '<dt class="col-sm-3" id="salary-header">Salary</dt>')
        self.assertNotContains(response_cand, '<dt class="col-sm-3" id="salary-header">Salary</dt>')

    def test_employee_candidate_salary_json_no_perms(self):
        response_emp = self.custom_client.get(reverse('skillsearch:employeedetails', kwargs={'pk': 1}))
        response_cand = self.custom_client.get(reverse('skillsearch:candidatedetails', kwargs={'pk': 1}))
        self.assertNotContains(response_emp, '"salary":')
        self.assertNotContains(response_cand, '"salary":')

    def test_employee_candidate_salary(self):
        add_review = Permission.objects.get(name='Can add review')
        change_review = Permission.objects.get(name='Can change review')
        delete_review = Permission.objects.get(name='Can delete review')
        add_salary = Permission.objects.get(name='Can add salary')
        change_salary = Permission.objects.get(name='Can change salary')
        delete_salary = Permission.objects.get(name='Can delete salary')
        add_candidate_salary = Permission.objects.get(name='Can add candidate salary')
        change_candidate_salary = Permission.objects.get(name='Can change candidate salary')
        delete_candidate_salary = Permission.objects.get(name='Can delete candidate salary')
        self.user.user_permissions.add(add_review, change_review, delete_review, add_salary, change_salary, delete_salary, add_candidate_salary, change_candidate_salary, delete_candidate_salary)
        response_emp = self.custom_client.get(reverse('skillsearch:employeedetailspage', kwargs={'pk': 1}))
        response_cand = self.custom_client.get(reverse('skillsearch:candidatedetailspage', kwargs={'pk': 1}))
        self.assertContains(response_emp, 'id="salary-header">')
        self.assertContains(response_cand, 'id="salary-header">')

    def test_employee_salary_json(self):
        add_review = Permission.objects.get(name='Can add review')
        change_review = Permission.objects.get(name='Can change review')
        delete_review = Permission.objects.get(name='Can delete review')
        add_salary = Permission.objects.get(name='Can add salary')
        change_salary = Permission.objects.get(name='Can change salary')
        delete_salary = Permission.objects.get(name='Can delete salary')
        add_candidate_salary = Permission.objects.get(name='Can add candidate salary')
        change_candidate_salary = Permission.objects.get(name='Can change candidate salary')
        delete_candidate_salary = Permission.objects.get(name='Can delete candidate salary')
        self.user.user_permissions.add(add_review, change_review, delete_review, add_salary, change_salary, delete_salary, add_candidate_salary, change_candidate_salary, delete_candidate_salary)
        response_emp = self.custom_client.get(reverse('skillsearch:employeedetails', kwargs={'pk': 1}))
        response_cand = self.custom_client.get(reverse('skillsearch:candidatedetails', kwargs={'pk': 1}))
        self.assertContains(response_emp, '"salary":')
        self.assertContains(response_cand, '"salary":')

    def test_candidate_details_info(self):
        response = self.custom_client.get(reverse('skillsearch:candidatedetailspage', kwargs={'pk': 1}))
        candidate = Candidate.objects.get(id=1)
        self.assertContains(response,
                            '<h4 class="empty-tab" id="employee-details-name" align="center">' + candidate.first_name + ' ' + candidate.last_name + '</h4>')
        self.assertContains(response,
                            '<img class="profile-picture-img rounded" id="profile-picture" src="/media/' + candidate.picture.name + '" alt="Card image cap">')
        self.assertNotContains(response, '<dt class="col-sm-1" id="team-header">Teams</dt>')
        self.assertNotContains(response, '<a class="nav-link" id="candidate-control-tab-button" href="#control" data-toggle="tab" role="tab" aria-controls="control" aria-selected="false">Control</a>')

    def test_candidate_details_control(self):
        permission = Permission.objects.get(name='Can change candidate')
        self.user.user_permissions.add(permission)
        self.user.save()
        response = self.custom_client.get(reverse('skillsearch:candidatedetailspage', kwargs={'pk': 1}))
        self.assertContains(response,
                               '<a class="nav-link" id="candidate-control-tab-button" href="#control" data-toggle="tab" role="tab" aria-controls="control" aria-selected="false">Control</a>')
        self.assertNotContains(response, '<button type="button" class="btn btn-success btn-hire" id="btn-hire" data-toggle="popover" data-placement="right">Hire</button>')

    def test_candidate_details_hire(self):
        change_candidate = Permission.objects.get(name='Can change candidate')
        delete_candidate = Permission.objects.get(name='Can delete candidate')
        add_employees = Permission.objects.get(name='Can add employees')
        self.user.user_permissions.add(change_candidate, delete_candidate, add_employees)
        self.user.save()
        response = self.custom_client.get(reverse('skillsearch:candidatedetailspage', kwargs={'pk': 1}))
        self.assertContains(response, '<button type="button" class="btn btn-success btn-hire" id="btn-hire" data-toggle="popover" data-placement="right">Hire</button>')

    def test_candidate_hire_view(self):
        delete_candidate = Permission.objects.get(name='Can delete candidate')
        add_employees = Permission.objects.get(name='Can add employees')
        self.user.user_permissions.add(delete_candidate, add_employees)
        self.user.save()
        candidate = Candidate.objects.get(id=1)
        candidate.phone_number = '+380779876543'
        candidate.email = 'test@example.com'
        candidate.save()
        response = self.client.get(reverse('skillsearch:hire', kwargs={'candidate_id': 1}))
        employee = Employees.objects.latest('pk')
        employee_skills = EmployeeSkills.objects.filter(employees=employee.id)
        self.assertEqual(employee.first_name, candidate.first_name)
        self.assertEqual(employee.last_name, candidate.last_name)
        self.assertEqual(employee.birthday, candidate.birthday)
        self.assertEqual(employee.phone_number, candidate.phone_number)
        self.assertEqual(employee.pemail, candidate.email)
        self.assertTrue(employee_skills)

    def test_simple_search(self):
        '''
        Test checks search with OR and AND conditions
        :return:
        '''
        for condition in ['or', 'and']:
            #Using quotes for skill1 in order to search for exact skill
            response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesjson'), {'page': 1, 'searchstring': 'skill0 "skill1"','condition': condition}).content.decode('utf-8'))
            employee_ids = list()
            #Test if search only outputs employees with two specified skills
            for employee in response_content['employees_list']:
                skill_names = list()
                employee_ids.append(employee['id'])
                for skill in employee['skillset']:
                    skill_names.append(skill['name'])
                if condition == 'or':
                    self.assertTrue('Skill0' in skill_names or 'Skill1' in skill_names)
                elif condition == 'and':
                    self.assertTrue('Skill0' in skill_names and 'Skill1' in skill_names)
            #Check for duplicates
            self.assertTrue(len(employee_ids) == len(set(employee_ids)))

    def test_complex_search(self):
        '''
        Test checks complex search functionality
        :return:
        '''
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeescomplexjson'),
                                                             {'page': 1, 'search0': '"Compl1" "SCompl3"', 'search1': '"SCompl1"'}).content.decode('utf-8'))
        self.assertEqual(len(response_content['employees_list']), 1)
        for employee in response_content['employees_list']:
            self.assertEqual(employee['first_name'], 'Somplname')

    #SIMPLE

    def test_simple_search_wrong_skills(self):
        '''
        Check whether search returns nothing if incorrect skills name is entered
        :return:
        '''
        for condition in ['or', 'and']:
            response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesjson'),
                                                                 {'page': 1, 'searchstring': 'unbelievablylongrequest',
                                                                  'condition': condition}).content.decode('utf-8'))
            self.assertTrue(len(response_content['employees_list']) == 0)

    def test_simple_search_return_all(self):
        for condition in ['or', 'and']:
            response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesjson'),
                                                                 {'page': 1, 'searchstring': '',
                                                                  'condition': condition}).content.decode('utf-8'))
            self.assertTrue(len(response_content['employees_list']) > 0)

    #NAME

    def test_advanced_search_name_wrong(self):
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesadvancedjson'),
                                                                 {'page': 1, 'searchstring': 'nosuchname',
                                                                  'condition': 'or', 'search_by': 'name'}).content.decode('utf-8'))
        self.assertTrue(len(response_content['employees_list']) == 0)

    def test_advanced_search_name_return_all(self):
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesadvancedjson'),
                                                                 {'page': 1, 'searchstring': '',
                                                                  'condition': 'or', 'search_by': 'name'}).content.decode('utf-8'))
        self.assertTrue(len(response_content['employees_list']) > 0)

    #TEAM

    def test_advanced_search_team_wrong(self):
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesadvancedjson'),
                                                                 {'page': 1, 'searchstring': 'nosuchteam',
                                                                  'condition': 'or', 'search_by': 'team'}).content.decode('utf-8'))
        self.assertTrue(len(response_content['employees_list']) == 0)

    def test_advanced_search_team_return_all(self):
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesadvancedjson'),
                                                                 {'page': 1, 'searchstring': '',
                                                                  'condition': 'or', 'search_by': 'team'}).content.decode('utf-8'))
        self.assertTrue(len(response_content['employees_list']) > 0)

    #POSITION

    def test_advanced_search_position_wrong(self):
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesadvancedjson'),
                                                                 {'page': 1, 'searchstring': 'nosuchposition',
                                                                  'condition': 'or', 'search_by': 'position'}).content.decode('utf-8'))
        self.assertTrue(len(response_content['employees_list']) == 0)

    def test_advanced_search_position_return_all(self):
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesadvancedjson'),
                                                                 {'page': 1, 'searchstring': '',
                                                                  'condition': 'or', 'search_by': 'position'}).content.decode('utf-8'))
        self.assertTrue(len(response_content['employees_list']) > 0)

    #COMPLEX

    def test_complex_search_skill_wrong(self):
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeescomplexjson'),
                                                             {'page': 1, 'search0': 'incorrectskill onemoreskill', 'search1': '"anotherincorrectskill"'}).content.decode('utf-8'))
        self.assertTrue(len(response_content['employees_list']) == 0)

    def test_complex_search_skill_return_all(self):
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeescomplexjson'),
                                                             {'page': 1, 'search0': ''}).content.decode('utf-8'))
        self.assertTrue(len(response_content['employees_list']) > 0)

    def test_advanced_search_by_name(self):
        #STRANGE BEHAVIOR
        response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesadvancedjson'), {'page': 1, 'searchstring': 'Emp1 "Emp20"','condition': 'or', 'search_by': 'name'}).content.decode('utf-8'))
        first_names = list()
        last_names = list()
        for employee in response_content['employees_list']:
            first_names.append(employee['first_name'])
            last_names.append(employee['last_name'])
        self.assertTrue('Emp1' in first_names and 'Emp1' in last_names and 'Emp20' in first_names and 'Emp20' in last_names)
        self.assertFalse('Emp21' in first_names and 'Emp21' in last_names)

    def test_advanced_search_by_team(self):
        for condition in ['or', 'and']:
            response_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesadvancedjson'), {'page': 1, 'searchstring': self.team_names[3] + ' "' + self.team_names[6] + '"', 'condition': condition, 'search_by': 'team'}).content.decode('utf-8'))
            for employee in response_content['employees_list']:
                team_names = list()
                for team in employee['teams']:
                    team_names.append(team['name'])
                if condition == 'or':
                    self.assertTrue(self.team_names[3] in team_names or self.team_names[6] in team_names)
                elif condition == 'and':
                    self.assertTrue(self.team_names[3] in team_names and self.team_names[6] in team_names)

    def test_advanced_search_by_positions(self):
        for condition in ['or', 'and']:
            response_content = json.loads(self.custom_client.
                                          get(reverse('skillsearch:employeesadvancedjson'),
                                              {'page': 1, 'searchstring': self.position_names[3] + ' "' + self.position_names[6] + '"', 'condition': condition, 'search_by': 'position'}).
                                          content.decode('utf-8'))
            for employee in response_content['employees_list']:
                position_names = list()
                for position in employee['positions']:
                    position_names.append(position['name'])
                if condition == 'or':
                    self.assertTrue(self.position_names[3] in position_names or self.position_names[6] in position_names)
                elif condition == 'and':
                    self.assertTrue(self.position_names[3] in position_names and self.position_names[6] in position_names)

    def test_json_difference(self):
        '''
        Test compares json output for 2 pages and checks whether they are different
        :return:
        '''
        response_page1_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesjson'), {'page': 1}).content.decode('utf-8'))
        response_page2_content = json.loads(self.custom_client.get(reverse('skillsearch:employeesjson'), {'page': 2}).content.decode('utf-8'))
        #Test whether different data was provided on two pages
        self.assertNotEqual(response_page1_content, response_page2_content)

    def test_index_redirect(self):
        response = self.client.get(reverse('skillsearch:index'))
        self.assertEqual(response.status_code, 302)


class RoutingLogoutTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.client.logout()

    def test_404(self):
        response = self.client.get('skillsearch/linkwhichwillneverexist')
        self.assertEqual(response.status_code, 404)

    def test_employees_status_loggedout(self):
        response = self.client.get(reverse('skillsearch:employees'))
        self.assertEqual(response.status_code, 302)

    def test_employees_json_status_loggedout(self):
        response = self.client.get(reverse('skillsearch:employeesjson'))
        self.assertEqual(response.status_code, 302)

    def test_employees_json_details_status_loggedout(self):
        response = self.client.get(reverse('skillsearch:employeedetails', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 302)

class InterviewTestCase(TestCase):
    def setUp(self):

        self.client = Client()
        self.custom_client = CustomClient()

        #Creating interview results
        for r, c in {'approved': 'success', 'rejected': 'danger', 'on hold': 'warning'}.items():
            result = InterviewResult()
            result.name = r
            result.color = c
            result.save()

        #Creating candidates
        for e in range(5):
            candidate = Candidate(first_name = 'Cand' + str(e), last_name = 'Last' + str(e))
            candidate.save()

            for i in range(7):
                interview = Interview()
                # now = datetime.now()
                interview.candidate = candidate
                interview.candidate_id = candidate.id
                interview.result = InterviewResult.objects.order_by('?').first()
                interview.date = datetime.now()
                interview.short_description
                interview.description
                interview.save()

        #Creating test user
        self.username = 'testuser'
        self.userpassword = 'thisistestuserpassword123'
        self.user = User.objects.create_user(self.username)
        self.user.set_password(self.userpassword)
        self.user.save()

        #Loggin in
        self.client.login(username=self.username, password=self.userpassword)
        self.custom_client.login(username=self.username, password=self.userpassword)

    def test_hire_with_interviews(self):
        delete_candidate = Permission.objects.get(name='Can delete candidate')
        add_employees = Permission.objects.get(name='Can add employees')
        self.user.user_permissions.add(delete_candidate, add_employees)
        self.user.save()
        candidate_id = randint(1, 5)
        cinterviews = Interview.objects.filter(candidate__exact=candidate_id)
        response = self.client.get(reverse('skillsearch:hire', kwargs={'candidate_id': candidate_id}))
        einterviews = EmployeeInterview.objects.all()

        self.assertTrue(einterviews)
        #Check if candidate interviews were deleted
        self.assertEqual(cinterviews.count(), 0)
        #Check if there are exactly 7 interviews
        self.assertEqual(einterviews.count(), 7)
