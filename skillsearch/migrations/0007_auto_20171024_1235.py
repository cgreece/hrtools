# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-24 12:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('skillsearch', '0006_employees_thumbnail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employees',
            name='picture',
            field=models.ImageField(default='./plug.png', upload_to='./'),
        ),
        migrations.AlterField(
            model_name='employees',
            name='thumbnail',
            field=models.ImageField(default='./plug.png', editable=False, upload_to='./'),
        ),
    ]
