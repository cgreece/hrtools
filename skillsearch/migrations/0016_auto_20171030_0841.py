# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-30 08:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skillsearch', '0015_remove_employees_skillset'),
    ]

    operations = [
        migrations.RenameField(
            model_name='employees',
            old_name='skillset1',
            new_name='skillset',
        ),
    ]
