# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-11 12:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('skillsearch', '0002_auto_20171005_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='employees',
            name='picture',
            field=models.ImageField(default='skillsearch/images/plug.svg', upload_to='skillsearch/images'),
        ),
    ]
