from django.apps import AppConfig


class SkillsearchConfig(AppConfig):
    name = 'skillsearch'
