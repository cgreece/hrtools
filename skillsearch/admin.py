from django.contrib import admin
from .models import Skills, Employees, Team, Candidate, Position, Review, Salary, CandidateSalary, Interview, InterviewResult, EmployeeInterview, ComplianceResult

class SkillsAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    fieldsets = [
        (None, {'fields': ['name']}),
    ]
    search_fields = ['name']

class TeamAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    fieldsets = [
        (None, {'fields': ['name']}),
    ]
    search_fields = ['name']

class PositionAdmin(admin.ModelAdmin):
    list_display = ('name', )
    list_display_links = ('name',)
    fieldsets = [
        (None, {'fields': ['name']}),
    ]
    search_fields = ['name']

class SkillsInline(admin.TabularInline):
    model = Employees.skillset.through
    rows = ['name', 'level']
    extra = 1

class CandidateSkillsInline(admin.TabularInline):
    model = Candidate.skillset.through
    rows = ['name']
    extra = 1

class TeamInline(admin.TabularInline):
    model = Employees.teams.through
    extra = 1

class PositionInline(admin.TabularInline):
    model = Employees.position.through
    extra = 1

class SalaryInline(admin.TabularInline):
    model = Salary
    extra = 1
    max_num = 1

class CandidateSalaryInline(admin.TabularInline):
    model = CandidateSalary
    extra = 1
    max_num = 1

class SkillsearchAdmin(admin.ModelAdmin):
    '''
    Class is used for employees, also it is parent of candidate class
    '''
    list_display = ('group_names', 'active')
    list_display_links = ('group_names',)
    fieldsets = [
        (None,               {'fields': ['first_name', 'last_name', 'compliance', 'birthday', 'phone_number', 'pemail', 'cemail', 'skype', 'facebook', 'linkedin', 'twitter', 'active', 'picture', 'cv']}),
    ]
    search_fields = ['first_name', 'last_name']
    inlines = [SkillsInline, TeamInline, PositionInline, SalaryInline]

    def group_names(self, obj):
        return "%s %s" % (obj.first_name, obj.last_name)
    group_names.short_description = 'Name'

    def save_model(self, request, obj, form, change):
        '''
        Sets name of creator for profile
        '''
        if not change:
            obj.added_by = request.user
        obj.save()

class CandidateAdmin(SkillsearchAdmin):
    list_display = ('group_names',)
    fieldsets = [
        (None,               {'fields': ['first_name', 'last_name', 'birthday', 'phone_number', 'email', 'skype', 'facebook', 'linkedin', 'twitter', 'picture', 'cv']}),
    ]
    inlines = [CandidateSkillsInline, CandidateSalaryInline]

class InterviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'candidate')
    list_display_links = ('id', 'candidate')
    fieldsets = [
        (None, {
            'fields': ['candidate', 'result', 'date', 'teams', 'interviewers', 'short_description', 'description']}),
    ]

    #Widget for multiple interviewers select
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        kwargs['widget'] = admin.widgets.FilteredSelectMultiple(
            db_field.verbose_name,
            db_field.name in self.filter_vertical
        )

        return super(admin.ModelAdmin, self).formfield_for_manytomany(
            db_field, request=request, **kwargs)

class EmployeeInterviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'employee')
    list_display_links = ('id', 'employee')
    fieldsets = [
        (None, {
            'fields': ['employee', 'result', 'date', 'teams', 'interviewers', 'short_description', 'description']}),
    ]

    #Widget for multiple interviewers select
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        kwargs['widget'] = admin.widgets.FilteredSelectMultiple(
            db_field.verbose_name,
            db_field.name in self.filter_vertical
        )

        return super(admin.ModelAdmin, self).formfield_for_manytomany(
            db_field, request=request, **kwargs)

class ReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'employee')
    list_display_links = ('id', 'employee')
    fieldsets = [
        (None, {
            'fields': ['employee', 'date', 'responsilities_feedback', 'responsilities_promises',
                       'team_feedback', 'team_promises',
                       'office_feedback', 'office_promises',
                       'other_feedback', 'other_promises']}),
    ]

class InterviewResultAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'color')
    list_display_links = ('id', 'name')
    fieldsets = [
        (None, {
            'fields': ['name', 'color']}),
    ]

class ComplianceResultAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'color')
    list_display_links = ('id', 'name')
    fieldsets = [
        (None, {
            'fields': ['name', 'color']}),
    ]

admin.site.register(Skills, SkillsAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(Employees, SkillsearchAdmin)
admin.site.register(Candidate, CandidateAdmin)
admin.site.register(Position, PositionAdmin)
admin.site.register(Interview, InterviewAdmin)
admin.site.register(EmployeeInterview, EmployeeInterviewAdmin)
admin.site.register(InterviewResult, InterviewResultAdmin)
admin.site.register(ComplianceResult, ComplianceResultAdmin)
admin.site.register(Review, ReviewAdmin)