from django.db import models
from django.core.files.base import ContentFile
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from djmoney.models.fields import MoneyField
from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse
from PIL import Image
from io import BytesIO
from .validators import validate_file_extension
import os

class Skills(models.Model):
    class Meta:
        verbose_name_plural = "skills"
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

class Team(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

class Position (models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

class ProfileBase(models.Model):
    '''
    Parent abstract class for Employees and Candidate classes
    '''
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    birthday = models.DateField(blank=True, null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField('Phone', validators=[phone_regex], max_length=15, blank=True)
    skype = models.CharField(max_length=30, blank=True)
    picture = models.ImageField(upload_to='./', default='./plug.png')
    thumbnail = models.ImageField(upload_to='./', editable=False, default='./plug.png')
    cv = models.FileField(upload_to='./cv/', validators=[validate_file_extension], blank=True, verbose_name='CV')
    added_by = models.ForeignKey(User, blank=True, null=True)
    #Social
    facebook = models.URLField(blank=True, max_length=128)
    linkedin = models.URLField(blank=True, max_length=128)
    twitter = models.URLField(blank=True, max_length=128)

    class Meta:
        abstract = 'True'

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    def save_default(self, *args, **kwargs):
        super(ProfileBase, self).save(*args, **kwargs)

    def save(self, *args, **kwargs):
        super(ProfileBase, self).save(*args, **kwargs)
        #Check whether default picture is set in order to prevent converting during every save
        if str(self.picture.name) != str(self.thumbnail.name) and '_thumb' not in self.thumbnail.name:
            if not self.make_thumbnail():
                raise Exception('Could not create thumbnail - is the file type valid?')

    def file_icon(self):
        extension = self.cv.name.split('.')[-1]
        if extension == 'pdf':
            return 'fa-file-pdf-o'
        elif extension in ['doc', 'docx']:
            return 'fa-file-word-o'
        return 'fa-file-file-o'

    def make_thumbnail(self):
        '''
        Create and save the thumbnail for the photo (simple resize with PIL).
        '''

        try:
            image = Image.open(self.picture)
        except:
            return False

        image.thumbnail((self.picture.width, settings.THUMB_SIZE), Image.ANTIALIAS)

        # Path to save to, name, and extension
        thumb_name, thumb_extension = os.path.splitext(self.picture.name)
        thumb_extension = thumb_extension.lower()

        thumb_filename = thumb_name + '_thumb' + thumb_extension
        if thumb_extension in ['.jpg', '.jpeg']:
            FTYPE = 'JPEG'
        elif thumb_extension == '.gif':
            FTYPE = 'GIF'
        elif thumb_extension == '.png':
            FTYPE = 'PNG'
        else:
            return False  # Unrecognized file type

        temp_thumb = BytesIO()
        # image.save(settings.MEDIA_ROOT + '/' + thumb_filename, FTYPE)
        image.save(temp_thumb, FTYPE)
        temp_thumb.seek(0)

        self.thumbnail.save('./' + thumb_filename, ContentFile(temp_thumb.read()), save=False)
        temp_thumb.close()
        self.save_default()
        return True

#Employees

class Employees(ProfileBase):
    class Meta:
        verbose_name_plural = "employees"

    pemail = models.EmailField('Personal Email', max_length=100, blank=True)
    cemail = models.EmailField('Corporate Email', max_length=100, blank=True)
    position = models.ManyToManyField(Position)
    active = models.BooleanField(default=True)
    compliance = models.ForeignKey('ComplianceResult', null=True)
    teams = models.ManyToManyField(Team)
    skillset = models.ManyToManyField(Skills, through='EmployeeSkills', related_name='employees')

    def serialize(self):
        skillset = self.skillset.all()
        skills = list()
        levels = self.get_levels()
        for index, skill in enumerate(skillset):
            skills.append({'name': skill.name, 'level': levels[index], 'id': skill.id})
            skillset[index].level = levels[index]
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'birthday': self.birthday.strftime('%b. %d, %Y') if self.birthday else self.birthday,
            'phone': self.phone_number,
            'pemail': self.pemail,
            'cemail': self.cemail,
            'skype': self.skype,
            'linkedin': self.linkedin,
            'facebook': self.facebook,
            'twitter': self.twitter,
            'active': self.active,
            'picture':  self.thumbnail.url,
            'teams': self.teams.all(),
            'positions': self.position.all(),
            'cv': self.cv.url if self.cv else '',
            'cvicon': self.file_icon() if self.cv else '',
            'skillset': skills,
            'added_by': self.added_by.first_name + ' ' + self.added_by.last_name if self.added_by else '',
            'compliance': self.compliance if self.compliance else '',
        }

    def get_levels(self):
        #Seems like not a good idea, probably need to remake
        employeeskill = EmployeeSkills.objects.filter(employees__exact=self.id)
        return [s.level for s in employeeskill]

class ComplianceResult(models.Model):
    COLORS = (
        ('primary', 'Primary (blue)'),
        ('secondary', 'Secondary (grey)'),
        ('success', 'Success (green)'),
        ('danger', 'Danger (red)'),
        ('warning', 'Warning (yellow)'),
        ('info', 'Info (light blue)'),
        ('light', 'Light (light grey)'),
        ('dark', 'Dark (almost black)'),
        ('white', 'White'),
    )

    name = models.CharField(max_length=30)
    color = models.CharField(max_length=10, choices=COLORS, default='success')

    def __str__(self):
        return self.name

class EmployeeSkills(models.Model):
    class Meta:
        verbose_name_plural = "Skills"
    employees = models.ForeignKey(Employees)
    skills = models.ForeignKey(Skills)
    level = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(10)], default=0)

class Review(models.Model):
    employee = models.ForeignKey(Employees)
    date = models.DateField(blank=True, null=True)
    responsilities_feedback = models.TextField(max_length=4000)
    responsilities_promises = models.TextField(max_length=4000)
    team_feedback = models.TextField(max_length=4000)
    team_promises = models.TextField(max_length=4000)
    office_feedback = models.TextField(max_length=4000)
    office_promises = models.TextField(max_length=4000)
    other_feedback = models.TextField(max_length=4000)
    other_promises = models.TextField(max_length=4000)

    def serialize(self):
        return {
            'id': self.id,
            'date': self.date.strftime('%b. %d, %Y') if self.date else self.date,
            'responsilities_feedback': self.responsilities_feedback,
            'responsilities_promises': self.responsilities_promises,
            'team_feedback': self.team_feedback,
            'team_promises': self.team_promises,
            'office_feedback': self.office_feedback,
            'office_promises': self.office_promises,
            'other_feedback': self.other_feedback,
            'other_promises': self.other_promises,
            'link': self.get_link()
        }

    def get_link(self):
        return reverse('skillsearch:review', args=(self.id,))

    def __str__(self):
        return self.employee.first_name + ' ' + self.employee.last_name

class Salary(models.Model):
    class Meta:
        verbose_name_plural = "salary"
    employee = models.ForeignKey(Employees)
    current_salary = MoneyField(max_digits=10, decimal_places=2, default_currency='EUR')
    desired_salary = MoneyField(max_digits=10, decimal_places=2, default_currency='EUR')

    def __str__(self):
        return self.employee.first_name + ' ' + self.employee.last_name

    def serialize(self):
        return {
            'id': self.id,
            'current_salary': self.current_salary,
            'desired_salary': self.desired_salary,
        }

#Candidates

class Candidate(ProfileBase):
    email = models.EmailField(max_length=100, blank=True)
    skillset = models.ManyToManyField(Skills, through='CandidateSkills', related_name='candidates')

    def serialize(self):
        skillset = self.skillset.all()
        skills = list()
        levels = self.get_levels()
        for index, skill in enumerate(skillset):
            skills.append({'name': skill.name, 'level': levels[index], 'id': skill.id})
            skillset[index].level = levels[index]
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'birthday': self.birthday,
            'phone': self.phone_number,
            'email': self.email,
            'skype': self.skype,
            'linkedin': self.linkedin,
            'facebook': self.facebook,
            'twitter': self.twitter,
            'picture':  self.thumbnail.url,
            'skillset': skills,
            'cv': self.cv.url if self.cv else '',
            'cvicon': self.file_icon() if self.cv else '',
            'added_by': self.added_by.first_name + ' ' + self.added_by.last_name if self.added_by else '',
        }

    def get_levels(self):
        candidateskill = CandidateSkills.objects.filter(candidates__exact=self.id)
        return [s.level for s in candidateskill]

class CandidateSkills(models.Model):
    class Meta:
        verbose_name_plural = "skills"
    candidates = models.ForeignKey(Candidate)
    skills = models.ForeignKey(Skills)
    level = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(10)], default=0)

class CandidateSalary(models.Model):
    class Meta:
        verbose_name_plural = "candidate salary"
    candidate = models.ForeignKey(Candidate)
    salary = MoneyField(max_digits=10, decimal_places=2, default_currency='EUR')

    def __str__(self):
        return self.candidate.first_name + ' ' + self.candidate.last_name

    def serialize(self):
        return {
            'id': self.id,
            'salary': self.salary
        }

class InterviewResult(models.Model):
    COLORS = (
        ('primary', 'Primary (blue)'),
        ('secondary', 'Secondary (grey)'),
        ('success', 'Success (green)'),
        ('danger', 'Danger (red)'),
        ('warning', 'Warning (yellow)'),
        ('info', 'Info (light blue)'),
        ('light', 'Light (light grey)'),
        ('dark', 'Dark (almost black)'),
        ('white', 'White'),
    )

    name = models.CharField(max_length=30)
    color = models.CharField(max_length=10, choices=COLORS, default='success')

    def __str__(self):
        return self.name

class BaseInterview(models.Model):
    date = models.DateField(blank=True, null=True)
    teams = models.ManyToManyField(Team)
    interviewers = models.ManyToManyField(Employees)
    short_description = models.CharField(max_length=60)
    description = models.TextField(max_length=1000)
    result = models.ForeignKey(InterviewResult)

    class Meta:
        abstract = 'True'

    def serialize(self):
        serialization_dict = {
            'id': self.id,
            'date': self.date.strftime('%b. %d, %Y') if self.date else self.date,
            'short_description': self.short_description,
            'description': self.description,
            'result': self.result,
        }
        return serialization_dict

class Interview(BaseInterview):
    candidate = models.ForeignKey(Candidate)

    def __str__(self):
        return '%s %s' % (self.candidate.first_name, self.candidate.last_name)

    def get_link(self):
        return reverse('skillsearch:interview', args=(self.id,))

    def serialize(self):
        serialization_dict = super(Interview, self).serialize()
        serialization_dict['link'] = self.get_link()
        return serialization_dict

class EmployeeInterview(BaseInterview):
    employee = models.ForeignKey(Employees, related_name='hired_employee')

    def __str__(self):
        return '%s %s' % (self.employee.first_name, self.employee.last_name)

    def get_link(self):
        return reverse('skillsearch:einterview', args=(self.id,))

    def serialize(self):
        serialization_dict = super(EmployeeInterview, self).serialize()
        serialization_dict['link'] = self.get_link()
        return serialization_dict

