from django.conf.urls import url
from django.views.generic import RedirectView
from . import views


app_name = 'skillsearch'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(url = 'employees/'), name='index'),
    url(r'^skills/', views.SkillsView.as_view(), name='skills'),
    url(r'^employees/$', views.EmployeesView.as_view(), name='employees'),
    url(r'^candidates/$', views.CandidatesView.as_view(), name='candidates'),
    url(r'^employees/(?P<pk>[0-9]+)/$', views.EmployeeDetailView.as_view(), name='employeedetailspage'),
    url(r'^candidates/(?P<pk>[0-9]+)/$', views.CandidateDetailView.as_view(), name='candidatedetailspage'),
    url(r'^employeesjson/', views.EmployeesJSONView.as_view(), name='employeesjson'),
    url(r'^candidatesjsonview/', views.CandidatesJSONView.as_view(), name='candidatesjson'),
    url(r'^employeesadvancedjson/', views.EmployeesAdvancedJSONView.as_view(), name='employeesadvancedjson'),
    url(r'^candidatesadvancedjson/', views.CandidatesAdvancedJSONView.as_view(), name='candidatesadvancedjson'),
    url(r'^employeescomplexjson/', views.EmployeesComplexJSONView.as_view(), name='employeescomplexjson'),
    url(r'^candidatescomplexjson/', views.CandidateComplexJSONView.as_view(), name='candidatescomplexjson'),
    url(r'^details/(?P<pk>[0-9]+)/$', views.EmployeeJSONDetailView.as_view(), name='employeedetails'),
    url(r'^cdetails/(?P<pk>[0-9]+)/$', views.CandidateJSONDetailView.as_view(), name='candidatedetails'),
    url(r'^details/$', RedirectView.as_view(pattern_name="employees"), name='employeedetailsredirect'),
    url(r'^cdetails/$', RedirectView.as_view(pattern_name="candidates"), name='candidatedetailsredirect'),
    url(r'^hire/(?P<candidate_id>[0-9]+)/$', views.HireCandidateView.as_view(), name='hire'),
    url(r'^hire/$', RedirectView.as_view(pattern_name="hire"), name='hireredirect'),
    url(r'^interviews/$', views.InterviewListView.as_view(), name='interviews'),
    url(r'^interviews/(?P<candidate_id>[0-9]+)/$', views.InterviewListView.as_view(), name='interviews'),
    url(r'^einterviews/(?P<employee_id>[0-9]+)/$', views.EmployeeInterviewListView.as_view(), name='einterviews'),
    url(r'^interview/$', views.InterviewDetailView.as_view(), name='interviewplug'),
    url(r'^interview/(?P<pk>[0-9]+)/$', views.InterviewDetailView.as_view(), name='interview'),
    url(r'^einterview/(?P<pk>[0-9]+)/$', views.EmployeeInterviewDetailView.as_view(), name='einterview'),
    url(r'^reviews/(?P<employee_id>[0-9]+)/$', views.ReviewListView.as_view(), name='reviews'),
    url(r'^review/(?P<pk>[0-9]+)/$', views.ReviewDetailView.as_view(), name='review'),
]
