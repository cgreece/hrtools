from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render, reverse

def login_view(request):
    post = request.POST
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        if 'next' in request.session:
            return redirect(request.session['next'])
        else:
            return redirect('hrtools:index')
    else:
        # Return an 'invalid login' error message.
        return render(request, 'hrlogin/unsuccessful_auth.html')

def logout_view(request):
    logout(request)
    return redirect('hrtools:index')
