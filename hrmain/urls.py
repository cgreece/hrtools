from django.conf.urls import url, include
from . import views

app_name = 'hrtools'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^config/password', views.PasswordChangeView.as_view(), name='password_config'),
    url(r'^config/settings', views.UserSettingsView.as_view(), name='user_settings'),
]