from django import template

register = template.Library()

@register.filter
def get_dict_key(dict, key):
    if dict.get(key) is not None:
        return dict.get(key)
    else:
        return False