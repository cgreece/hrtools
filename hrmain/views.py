from django.views import generic
from .forms import UserPasswordForm, UserSettingsForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms.utils import ErrorList
from django.contrib.auth.models import User
from django.contrib import messages
from django.shortcuts import redirect, reverse

class IndexView(generic.TemplateView):
    template_name = 'hrmain/base.html'
    context_object_name = 'index'
    
    def dispatch(self, request, *args, **kwargs):
        #Redirecting to url which was entered before login
        for next in ['next', 'redirect_to']:
            if next in self.request.GET:
                self.request.session['next'] = self.request.GET[next]
        if self.request.user.is_authenticated and 'next' not in self.request.session:
            return redirect(reverse('skillsearch:employees'))
        return super(IndexView, self).dispatch(request, *args, **kwargs)

class UserSettingsView(LoginRequiredMixin, generic.FormView):
    template_name = 'hrmain/settings.html'
    form_class = UserSettingsForm
    success_url = 'settings'
    login_url = '/'
    
    def form_valid(self, form):
        self.user = User.objects.get(id=self.request.user.id)
        if self.user.check_password(self.request.POST['password']):
            if self.request.POST['login']:
                self.user.username = self.request.POST['login']
            if self.request.POST['email']:
                self.user.email = self.request.POST['email']
            if self.request.POST['first_name']:
                self.user.first_name = self.request.POST['first_name']
            if self.request.POST['last_name']:
                self.user.last_name = self.request.POST['last_name']
            self.user.save()
            messages.success(self.request, 'Settings have been saved')
        else:
            #Throwing custom error for field and triggering form_invalid to show the error
            form.add_error('password', 'Password is invalid')
            return self.form_invalid(form)
        return super(UserSettingsView, self).form_valid(form)

class PasswordChangeView(LoginRequiredMixin, generic.FormView):
    template_name = 'hrmain/password.html'
    form_class = UserPasswordForm
    success_url = '/'
    login_url = '/'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'user': self.request.user, })
        return kwargs

    def form_valid(self, form):
        user = User.objects.get(id=self.request.user.id)
        user.set_password(self.request.POST['new_password2'])
        user.save()
        messages.success(self.request, 'Password has been changed')
        return super(PasswordChangeView, self).form_valid(form)