from django import forms
from django.contrib.auth.forms import PasswordChangeForm

class UserPasswordForm(PasswordChangeForm):
    pass

class UserSettingsForm(forms.Form):
    login = forms.CharField(required=False, max_length=50)
    email = forms.EmailField(required=False, max_length=50)
    first_name = forms.CharField(required=False, max_length=50)
    last_name = forms.CharField(required=False, max_length=50)
    password = forms.CharField(widget=forms.PasswordInput())
    pass
