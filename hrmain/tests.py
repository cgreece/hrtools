from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User

#Views

class SettingsTestCase(TestCase):
    '''
    Testing user settings
    '''
    def setUp(self):
        self.client = Client()
        self.username = 'testcase'
        self.password = 'password'
        user = User.objects.create_user(self.username)
        user.set_password(self.password)
        user.email = 'test@test.te'
        user.first_name = 'Test'
        user.last_name = 'Testcase'
        user.save()

    def test_settings_loggedout(self):
        '''
        Tesing access to 'settings' from without auth
        :return:
        '''
        response = self.client.get(reverse('hrtools:user_settings'))
        self.assertEqual(response.status_code, 302)

    def test_settings_loggedin(self):
        '''
        Tesing 'settings' page availability
        :return:
        '''
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(reverse('hrtools:user_settings'))
        self.assertEqual(response.status_code, 200)

    def test_settings_form(self):
        '''
        Checking whether all settings are changed by the view
        :return:
        '''
        self.client.login(username=self.username, password=self.password)
        new_email = 'tset@test.te'
        new_name = 'Test1'
        new_last_name = 'Testcase1'
        response = self.client.post(reverse('hrtools:user_settings'), {'login': self.username + '1', 'email': new_email, 'first_name': new_name, 'last_name': new_last_name, 'password': self.password})
        user = User.objects.get(id=1)
        self.assertEqual(user.username, self.username + '1')
        self.assertEqual(user.email, new_email)
        self.assertEqual(user.first_name, new_name)
        self.assertEqual(user.last_name, new_last_name)

    def test_settings_wrong_password(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.post(reverse('hrtools:user_settings'), {'login': self.username + '1', 'password': 'obviouslywrongpasssword', 'email': '', 'first_name': '', 'last_name': ''})
        user = User.objects.get(id=1)
        #if usernames do not match, then view accepts any password
        self.assertEqual(user.username, self.username)

    def test_change_password_form(self):
        '''
        Testing whether password is changed by view
        :return:
        '''
        self.client.login(username=self.username, password=self.password)
        new_password = 'ashasjkdh125'
        response = self.client.post(reverse('hrtools:password_config'), {'old_password': self.password, 'new_password1': new_password, 'new_password2': new_password})
        self.client.logout()
        self.client.login(username=self.username, password=new_password)
        response2 = self.client.get(reverse('hrtools:password_config'))
        self.assertEqual(response2.status_code, 200)
