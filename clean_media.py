#!/usr/bin/env python

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hrtools.settings")
import django
django.setup()
from skillsearch.models import Employees, Candidate

def remove_media():
    media_path = django.conf.settings.MEDIA_ROOT
    all_files = os.listdir(media_path)
    employees = Employees.objects.all()
    candidates = Candidate.objects.all()
    pictures = list()
    for employee in employees:
        pictures.append(employee.picture.name)
        pictures.append(employee.thumbnail.name)
    for candidate in candidates:
        pictures.append(candidate.picture.name)
        pictures.append(candidate.thumbnail.name)
    for file in all_files:
        if file not in pictures and file != 'plug.png' and os.path.isfile(media_path + '/' + file):
            os.remove(media_path + '/' + file)


remove_media()